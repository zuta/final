import images
def negative(image: list[list[tuple[int, int, int]]]):

    negative_image = [[0 for x in range(len(image[0]))] for y in range(len(image))]

    for x in range (len(image)):
        for y in range (len(image[x])):
            red, green, blue = image[x][y]

            inverted_red = 255 - red
            inverted_green = 255 - green
            inverted_blue = 255 - blue

            negative_image[x][y] = (inverted_red, inverted_green, inverted_blue)

    return negative_image
def main():
    fichero = input("Escriba la imagen a la que quiera invertir sus valores (R,G,B): ")

    img = images.read_img(fichero)
    imagen_cambiada = negative(img)
    images.write_img(imagen_cambiada, "archivo2.jpg")


if __name__ == '__main__':
    main()