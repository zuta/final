import images

def change_colors(image: list[list[tuple[int, int, int]]], to_change: tuple, to_change_to: tuple):
    for x in range(len(image)):     #el bucle recorre cada fila de la imagen
        for y in range(len(image[x])):      #el bucle recorre cada columna y de la fila x
            if image[x][y] == to_change:        #se compara si el pixel es igual con el nuevo color
                image[x][y] = to_change_to      # si son iguales los cambia por el nuevo color
    return image        #devuelve la imagen con los nuevos colores

def rotate_right(image: list[list[tuple[int, int, int]]]):
    # Rotate image 90 degrees clockwise
    rotated_image = [[0 for x in range(len(image))] for y in range(len(image[0]))]      #crea una matriz nueva con rgb 0,0,0 es decir negra completa con las dimensiones rotadas
    for x in range(len(image)):     #Comienza el bucle sobre cada fila X de la imagen original
        for y in range(len(image[x])):  #Comienza el bucle sobre cada columna Y de la fila X
            rotated_image[y][len(image)-1-x] = image[x][y]          #rotated_image[y] selecciona la fila según la coordenada Y original / [len(image)-1-x] selecciona la columna invertida según la coordenada X original / y se iguala a image[x][y] para darle color a la imagen rotada
    return rotated_image

def mirror(image):
    # Mirror image vertically (top to bottom)
    mirrored_image = [[0 for x in range(len(image[0]))] for y in range(len(image))]
    for x in range(len(image)):
        for y in range(len(image[x])):
            mirrored_image[len(image)-1-x][y] = image[x][y]
    return mirrored_image
def rotate_colors(image: list[list[tuple[int, int, int]]], increment: int):
    # Rotate colors by increment
    colors = [(r,g,b) for row in image for (r,g,b) in row]
    for i in range(len(colors)):
        r, g, b = colors[i]
        r = (r + increment) % 256
        g = (g + increment) % 256
        b = (b + increment) % 256
        colors[i] = (r,g,b)
    k = 0
    for i in range(len(image)):
        for j in range(len(image[i])):
            image[i][j] = colors[k]
            k += 1
    return image

def blur(image: list[list[tuple[int, int, int]]]):

    blurred_image = [[0 for x in range(len(image[0]))] for y in range(len(image))]

    for x in range(len(image)):
        for y in range(len(image[x])):
            red = 0
            green = 0
            blue = 0
            count = 0

            if y-1 >= 0:
                r, g, b = image[x][y-1]
                red += r
                green += g
                blue += b
                count += 1

            if y+1 < len(image[x]):
                r, g, b = image[x][y+1]
                red += r
                green += g
                blue += b
                count += 1

            if x-1 >= 0:
                r, g, b = image[x-1][y]
                red += r
                green += g
                blue += b
                count += 1

            if x+1 < len(image):
                r, g, b = image[x+1][y]
                red += r
                green += g
                blue += b
                count += 1

            blurred_image[x][y] = (int(red/count), int(green/count), int(blue/count))

    return blurred_image

def shift(image: list[list[tuple[int, int, int]]], horizontal: int, vertical: int):
    shifted_image = image
    for x in range(len(image)):         #recorre el eje x
        for y in range(len(image[0])):      #recorre el eje y
            new_x = (x + vertical) % len(image)     #declaramos que el nuevo x es el x original mas el desplazamiento vertical
            new_y = (y + horizontal) % len(image[0])    #declaramos que el nuevo y es el y original mas el desplazamiento horizontal

            shifted_image[x][y] = image[new_x][new_y]       #decimos que la imagen desplazada en new_x y new_y tenga color, por eso igualamos a image[x][y]
    return shifted_image        #devolvemos la imagen desplazada

def crop(image: list[list[tuple[int, int, int]]], x: int, y: int, width: int, height: int):
    # Crop image from (x, y) - (x + width, y + height)
    cropped_image = [[0 for i in range(width)] for j in range(height)]      #creamos una matriz vacia, valores 0,0,0 del tamaño del recorte
    for i in range(height):         #recorremos altura del recorte
        for j in range(width):      #recorre el ancho del recorte
            if x+i < len(image) and y+j < len(image[0]):
                cropped_image[i][j] = image[x+i][y+j]
    return cropped_image

def greyscale (image: list[list[tuple[int, int, int]]]):
    # Convert image to grayscale
    greyscale_image = [[0 for x in range(len(image[0]))] for y in range(len(image))]        #crear una matriz vacía del mismo tamaño que la imagen original
    for i in range(len(image)):
        for j in range(len(image[i])):
            r, g, b = image[i][j]
            grey = (r + g + b) // 3
            greyscale_image[i][j] = (grey, grey, grey)
    return greyscale_image

def filter (image: list[list[tuple[int, int, int]]], r:float, g:float, b: float):

    filtered_image = [[0 for x in range (len(image[0]))] for y in range (len(image))]
    for x in range(len(image)):
        for y in range(len(image[x])):
            red, green, blue = image[x][y]       #Sacar los valores red green blue en el punto X Y de image
            r_new = int(r * red)     #Multiplica el valor rojo por el factor r
            g_new = int(g * green)
            b_new = int(b * blue)

            if r_new > 255:
                r_new = 255
            elif g_new > 255:
                g_new = 255
            elif b_new > 255:
                b_new = 255

            filtered_image[x][y] = (r_new, g_new, b_new)

    return filtered_image

#_______Parte de pruebas________

img_de_prueba = images.read_img("cafe.jpg")
cambio =rotate_right(img_de_prueba)
images.write_img(cambio, "prueba_cafe.jpg")