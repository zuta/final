import images
import transforms

def main():

    fichero = input("Introduce el nombre de la imagen en formato RGB: ")
    funcion = input("De entre estas funciones\n--> change_colors\n"
                "--> rotate_right\n--> mirror\n--> rotate_colors\n--> blur\n"
                "--> shift\n--> crop\n--> greyscale\n--> filter\n"
                "¿Qué función quieres aplicar?:  ")

    img = images.read_img(fichero)

    if funcion == "change_colors" or funcion == "rotate_colors" or funcion == "shift":
        if funcion == "change_colors":
            valores = input("Tupla de colores a cambiar, separalos por coma de esta manera (r,g,b): ")
            list_valores = list(valores.split(","))
            list_valores = [int(x) for x in list_valores]       #cada elemento de la lista se convierte en un entero
            tupla_valores = tuple(list_valores)

            valores2 = input("Tupla de nuevos colores, separalos de esta forma (r,g,b):  ")
            list_valores2 = list(valores2.split(","))
            list_valores = [int(x) for x in list_valores2]
            tupla_valores2 = tuple(list_valores2)

            ejecutable = transforms.change_colors(img, tupla_valores, tupla_valores2)


        elif funcion == "rotate_colors":
            valor = int(input("Incremento de colores a aplicar: "))

            ejecutable = transforms.rotate_colors(valor)

        elif funcion == "shift":
            valor_horizontal = int(input("¿Qué desplazamiento horizontal quieres aplicar en píxeles?: "))
            valor_vertical = int(input("¿Qué desplazamiento vertical quieres aplicar en píxeles?: "))

            ejecutable = transforms.shift(img, valor_horizontal, valor_vertical)


    elif funcion == "crop" or funcion == "filter":
        if funcion == "crop":
            x = int(input("Coordenada x a cortar: "))
            y = int(input("Coordenada y a cortar: "))
            ancho = int(input("Ancho del recorte: "))
            alto = int(input("Alto del recorte: "))

            ejecutable = transforms.crop(img, x, y, ancho, alto)

        elif funcion == "filter":
            rojo = input("Valor del color rojo: ")
            rojo_float = float(rojo)
            verde = input("Valor del color verde: ")
            verde_float = float(verde)
            azul = input("Valor del color azul: ")
            azul_float = float(azul)

            ejecutable = transforms.filter(img, rojo_float, verde_float, azul_float)


    elif funcion == "rotate_right" or funcion == "mirror" or funcion == "blur" or funcion == "greyscale":
        if funcion == "rotate_right":
            ejecutable = transforms.rotate_right(img)

        if funcion == "mirror":
            ejecutable = transforms.mirror(img)

        if funcion == "blur":
            ejecutable = transforms.blur(img)

        if funcion == "greyscale":
            ejecutable = transforms.greyscale(img)

    print("---> Proceso realizado correctamente <---")

    images.write_img(ejecutable, fichero + "_trans.png")

if __name__ == '__main__':
    main()