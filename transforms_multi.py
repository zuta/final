import images
import transforms

def main():
    funcion = input("Nombre de la imagen y las funciones que quieres aplicar separadas por un espacio, y en caso de que sean numeros enteros, sepáralos por comas: ")
    funciones: list = funcion.split(" ")
    img = images.read_img(funciones[0])

    for i in funciones:
        if i == "change_colors":
            index = funciones.index(i)

            list_valores: list = funciones[index + 1].split(",")
            list_valores = [int(x) for x in list_valores]
            tupla_valores = tuple(list_valores)

            list_color:list = funciones[index + 1].split(",")
            list_color = [int(x) for x in list_color]
            tupla_color = tuple(list_color)
            ejecutable = transforms.change_colors(img, tupla_valores, tupla_color)

            img = ejecutable

        elif i == "rotate_colors":
            index = funciones.index(i)

            incremento = int(funciones[index + 1])
            img = transforms.rotate_colors(img, incremento)
            ejecutable = transforms.rotate_colors(img, incremento)

            img = ejecutable

        elif i == "crop":
            index = funciones.index(i)

            valores: list = funciones[index + 1].split(",")
            valores = [int(x) for x in valores]

            coord_x = valores[0]
            coord_y = valores[1]

            anchura = valores[2]
            altura = valores [3]

            ejecutable = transforms.crop(img, coord_x, coord_y, anchura, altura)

            img = ejecutable

        elif i == "filter":
            index = funciones.index(i)

            valores: list = funciones[index]
            valores = [float(x) for x in valores]

            red = valores[0]
            green = valores[1]
            blue = valores[2]

            ejecutable = transforms.filter(img, red, green, blue)
            img = ejecutable

        elif i == "rotate_right":
            ejecutable = transforms.rotate_right(img)
            img = ejecutable

        elif i == "mirror":
            ejecutable = transforms.mirror(img)
            img = ejecutable

        elif i == "blur":
            ejecutable = transforms.blur(img)
            img = ejecutable

        elif i == "greyscale":
            ejecutable = transforms.greyscale(img)
            img = ejecutable

    images.write_img(img, funciones[0] + "_trans.png")

if __name__ == '__main__':
    main()




