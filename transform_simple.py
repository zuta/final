import images
import transforms

def main():
    fichero = input("Introduce el nombre de la imagen en formato RGB: ")
    funcion = input("¿Qué función que quieres que se aplique de entre estas: rotate_right, mirror, blur, greyscale. ")

    img = images.read_img(fichero)

    if funcion == "rotate_right":
        fichero_cambiado = transforms.rotate_right(img)

    if funcion == "mirror":
        fichero_cambiado = transforms.mirror(img)

    if funcion == "blur":
        fichero_cambiado = transforms.blur(img)

    if funcion == "greyscale":
        fichero_cambiado = transforms.greyscale(img)

    images.write_img(fichero_cambiado, fichero + "_trans.png")

if __name__ == '__main__':
    main()