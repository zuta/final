#HASHTAG CONVOCATORIA ENERO
Christian Daniel Zuta Ramos - cd.zuta.2023@alumnos.urjc.es
Enlace al vídeo --> https://youtu.be/i41Ct1NJd4c

-Requisitos mínimos:
change_colors, rotate_right, mirror, rotate_colors, blur, shift, crop, greyscale, filter
transforms, transforms_simple, transforms_args, transforms_multi

-Requisitos opcionales:
Método negative

-Requisitos opcionales propios:
Un archivo opcional donde se ha creado un método negative que lo que hace es a la imagen dada en RGB devuelva una imagen
de salida con los valores negativos RGB.